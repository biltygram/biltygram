val ScalatraVersion = "2.6.1"

organization := "com.example"

name := "BiltyGram"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.12.3"

resolvers += Classpaths.typesafeReleases


libraryDependencies ++= Seq(
  "org.scalatra" %% "scalatra" % ScalatraVersion,
  "org.scalatra" %% "scalatra-scalatest" % ScalatraVersion % "test",
  "ch.qos.logback" % "logback-classic" % "1.1.5" % "runtime",
  "org.eclipse.jetty" % "jetty-webapp" % "9.2.15.v20160210" % "container",
  "javax.servlet" % "javax.servlet-api" % "3.1.0" % "provided",
  "com.google.cloud" % "google-cloud" % "0.28.0-alpha",
  "com.google.cloud" % "google-cloud-datastore" % "1.12.0",
  "com.google.cloud" % "google-cloud-storage" % "1.11.0",
  "org.mongodb.scala" %% "mongo-scala-driver" % "1.2.1",
  "org.mongodb" %% "casbah" % "3.1.1",
  "org.json4s"   %% "json4s-jackson" % "3.5.2",
  "com.sksamuel.scrimage" % "scrimage-core_2.12" % "3.0.0-alpha1"
)

enablePlugins(SbtTwirl)
enablePlugins(ScalatraPlugin)
