package com.example.app

import org.scalatra._
import com.google.cloud.storage._
import java.io._

import org.scalatra.CorsSupport
import org.json4s._
import org.json4s.jackson.JsonMethods._
import com.mongodb.casbah.Imports._
import com.google.auth.oauth2.ServiceAccountCredentials

class MyScalatraServlet extends ScalatraServlet with CorsSupport {

  implicit val formats = org.json4s.DefaultFormats

  val mongoClient =  MongoClient("localhost", 27017)
  val db = mongoClient("local")

  val storage: Storage = StorageOptions.newBuilder()
    .setCredentials(ServiceAccountCredentials.fromStream(new FileInputStream("src/keyfile.json")))
    .build()
    .getService();

  options("/*") {
    response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"))
  }

  //=========================================== F U N C T I O N =======================================================

  def getPic(username: String): List[String] = {
    val getPic = db("pictures").find(MongoDBObject("username" -> username)).toList
    for( pic <- getPic) yield pic.get("picId").toString
  }

  def getMediaLink(picId: List[String]): List[String] ={
    picId.map( x => storage.get(BlobId.of("bilty-gram", x)).getMediaLink)
  }

  //=========================================== P E O P L E ==========================================================

  post("/signup"){
    val data = request.body
    val json = parse(data)
    val username = (json \ "username").extract[String]
    val password = (json \ "password").extract[String]
    val collection = db("people")
    val b = MongoDBObject("username" -> username, "password" -> password)

    if ( db("people").find(MongoDBObject("username" -> username, "password" -> password)).count() == 0 ){
      collection.insert(b)
      Ok("Succesfully Sign Up")
    }
    else {
      NotFound("Email is invalid")
    }
  }

  post("/signin") {
    val data = request.body
    val json = parse(data)
    val username = (json \ "username").extract[String]
    val password = (json \ "password").extract[String]

    if ( db("people").find(MongoDBObject("username" -> username, "password" -> password)).count() == 1) {
      Ok("sign in")
    }
    else {
      NotFound("Wrong username or password")
    }
  }

  post("/home"){
    val data = request.body
    val json = parse(data)
    val username = (json \ "username").extract[String]
    val followWho = db("follow").find(MongoDBObject("username" -> username)).toList
    val listOfPeep = for( who <- followWho) yield who.get("following") //list of picture that user follow
    val listOfPic = listOfPeep.flatMap( x => getPic(x.toString))
    val listOfLink = getMediaLink(listOfPic)
    Ok(listOfLink)
  }

  post("/friendHome"){
    val data = request.body
    val json = parse(data)
    val username = (json \ "username").extract[String]
    val whofollows = db("follow").find(MongoDBObject("following" -> username)).count() //follower count
    val followWho = db("follow").find(MongoDBObject("username" -> username)).count() //following count
    Ok(List(whofollows, followWho))
  }

  //============================================= F O L L O W ==========================================================

  //click to following others
  post("/mayIFollow") {
    val data = request.body
    val json = parse(data)
    val collection = db("follow")
    val following = (json \ "followedUser").extract[String] // a// follow
    val username = (json \ "username").extract[String] // b
    val state = (json \ "state").extract[Boolean]

    if(!state){
      val a = MongoDBObject("username" -> username , "following" -> following)
      collection.insert(a)
      Ok("Succesfully followed")
    }else{
      val a = MongoDBObject("username" -> username , "following" -> following)
      collection.remove(a)
      Ok("Succesfully unfollowed")
    }
  }

  //get peep that user follow
  post("/following"){
    val data = request.body
    val json = parse(data)
    val username = (json \ "username").extract[String]
    val followWho = db("follow").find(MongoDBObject("username" -> username)).toList
    val ans = for( who <- followWho) yield who.get("following")
    Ok(ans)
  }

  //get who are your followers
  post("/follower") {
    val data = request.body
    val json = parse(data)
    val following = (json \ "username").extract[String]
    val whofollows = db("follow").find(MongoDBObject("following" -> following)).toList
    val ans = for( who <- whofollows) yield who.get("username")
    Ok(ans)
  }

  post("/checkFollow"){
    val data = request.body
    val json = parse(data)
    val username = (json \ "username").extract[String]
    val following = (json \ "followedUser").extract[String]

    if ( db("follow").find(MongoDBObject("username" -> username, "following" -> following)).count() > 0) {
      Ok("Yay! I followed this guy!")
    }
    else {
      NotFound("No! I didn't follow this guy")
    }
  }

  //============================================= C O M M E N T ==========================================================

  post( "/comment"){
    val data = request.body
    val json = parse(data)
    val picId = (json \ "picId").extract[String] //to what picture
    val comment = (json \ "comment").extract[String] //comment
    val username = (json \ "username").extract[String] //who comment
    val collection = db("comment")
    val b = MongoDBObject("picId" -> picId, "comment" -> comment, "username" -> username)
    collection.insert(b)

  }

  get( "/showcomment"){
    val data = request.body
    val json = parse(data)
    val picId = (json \ "picId").extract[String]
    val wholikes = db("comment").find(MongoDBObject("picId" -> picId)).toList
    val ans = for( who <- wholikes) yield who.get("comment")
    Ok(ans)
  }

  //============================================= L I K E ==========================================================

  //insert like
  post("/like") {
    val data = request.body
    val json = parse(data)
    val picId = (json \ "photoId").extract[String]
    val username = (json \ "username").extract[String]
    val state = (json \ "likedByMe").extract[Boolean]
    val collection = db("like")

    if (!state){
      val b = MongoDBObject("picId" -> picId, "username" -> username)
      collection.insert(b)
      Ok("Like cha")
    }
    else{
      val b = MongoDBObject("picId" -> picId, "username" -> username)
      collection.remove(b)
      Ok("Unlike la")
    }

  }

  //count how many likes & who likes
  post("/photoInfo") {
    val data = request.body
    val json = parse(data)
    val picId = (json \ "photoId").extract[String]
    val likes = db("like").find(MongoDBObject("picId" -> picId)).count()
    val user = db("pictures").find(MongoDBObject("picId" -> picId)).toList
    val userID = user.map( x => x.get("username"))
    val ans = (likes, userID.head)
    Ok(ans)
  }

  //who like that picture
  post("/whoLikes") {
    val data = request.body
    val json = parse(data)
    val picId = (json \ "photoId").extract[String]
    val whoLikes = db("like").find(MongoDBObject("picId" -> picId)).toList
    val ans = for( who <- whoLikes) yield who.get("username")
    Ok(ans)
  }

  post("/likeOrNot"){
    val data = request.body
    val json = parse(data)
    val picId = (json \ "photoId").extract[String]
    val username = (json \ "username").extract[String]
    val didILike = db("like").find(MongoDBObject("picId" -> picId, "username" -> username)).count()
    if (didILike == 1){
      Ok("Yes! I like this pic")
    }
    else{
      NotFound("I didn't like this pic")
    }
  }

  //============================================= P I C T U R E S ==========================================================

  //upload picture
  post("/upload") {
    import com.google.cloud.storage.BlobId
    import com.google.cloud.storage.BlobInfo
    import java.util.Base64

    val content = request.body
    val json = parse(content)
    val username = (json \ "username").extract[String]
    val uri = (json \ "uri").extract[String]
    val decode = java.net.URLDecoder.decode(uri, "UTF-8")
    val decoded = decode.substring(28)
    val realDecoded = Base64.getDecoder.decode(decoded)
    val id = db("pictures").count() + 1
//    val contentt = new FileInputStream(new File(content))
    val blobId = BlobId.of("bilty-gram", id.toString)
//    val deleted = storage.deleteAcl(blobId, User.ofAllAuthenticatedUsers)
    val blobInfo = BlobInfo.newBuilder(blobId).setContentType("image/jpeg").build
    storage.create(blobInfo, realDecoded)//array of byte
    val collection = db("pictures")
    val b = MongoDBObject("picId" -> id.toString, "username" -> username)
    collection.insert(b)
    val blob2 = storage.get(BlobId.of("bilty-gram", id.toString))
    // return the public download link

    Ok(blob2.getMediaLink)

  }

  //retrieve picture
  post("/retrieve"){
    import com.google.cloud.storage.BlobId

    val content = request.body
    val json = parse(content)
    val username = (json \ "username").extract[String]
    val getPic = db("pictures").find(MongoDBObject("username" -> username)).toList
    val ans = for( pic <- getPic) yield pic.get("picId")
    val listOfMediaLink = ans.map( x => storage.get(BlobId.of("bilty-gram", x.toString)).getMediaLink)
    Ok(listOfMediaLink)
  }

  post("/delete") {
    import com.mongodb.casbah.Imports._

    val data = request.body
//    val json = parse(data)
    //    val picId = (json \ "picId").extract[String]
    val picId = 2
    val query = MongoDBObject("picId" -> picId)
    val update = MongoDBObject(
      "$set" -> MongoDBObject("username" -> "0")
    )
    val delete = db("pictures").update(query, update)
  }
}
