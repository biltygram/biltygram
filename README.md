# BiltyGram #

## Build & Run ##

```sh
$ cd BiltyGram
$ ./sbt
> ~;jetty:stop;jetty:start
> browse
```

If `browse` doesn't launch your browser, manually open [http://localhost:8080/](http://localhost:8080/) in your browser.

```sh
-project
    -project
    -target
    build.properties
    plugins.sbt
-src
	-main
		-resources
		-scala
			-com.example.app
				-MyScalatraServlet: Most of API routes are in this file
			-ScalatraBootstrap
		-twirl
		-webapp
	-test
	-keyfile
-target
.gitignore
build.sbt
README.md
```